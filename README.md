﻿All of the original links to the skmMenu code were defunct, including but not limited to GotDotNet, skmMenu.com, and scottonwriting.com (Scott K. Mitchell is SKM).

The only place I could find the skmMenu.dll source code was:

http://www.gchandra.com/web/aspnet/skmmenu-23-consolidated-post-ie8-firefox-chrome.html

The CHM file found in the folder is arguably useless. It does not seem to work as a CHM file should because nothing loads in the preview panel.

Because the code came from an untrusted source, I manually read all code modules before checking it in. The project was also upgraded to Visual Studio 2015, but with only warnings.

For further reading: https://msdn.microsoft.com/en-us/library/aa478970.aspx